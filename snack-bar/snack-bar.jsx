'use strict';

import './snack-bar-style.scss';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {smartClass} from '~/components/js-helpers/class';
import escKeyAction from '~/components/js-helpers/keyboard/esc-key-action';
import matchWindowMedia from '~/components/js-helpers/browser/match-window-media';

const BASE_TOP_BOTTOM = 12;
const HIDDEN_CLASS = 'au-snack-bar-wrapper_hidden';

const SnackBarClose = props => {

    const {content, buttonText, onClose} = props;

    return (
        <div className='au-snack-bar-action'>
            <div className='au-snack-bar-close__content'>{content}</div>
            <div className='au-snack-bar-close__button'>
                <button
                    type='button'
                    className='au-btn au-btn_text au-snack-bar-close-btn'
                    onClick={onClose}>{buttonText}</button>
            </div>
        </div>
    );

};

SnackBarClose.defaultProps = {
    buttonText: 'закрыть',
};

class SnackBar extends PureComponent {

    componentDidMount () {
        this.hide();
    }

    componentDidUpdate () {

        const {show: curShow} = this.props;

        this.setBarCssClass();

        if (curShow) {
            this.show();
        } else {
            this.hide();
        }

    }

    setWrapperRef = wrapperRef => {
        this.wrapperRef = wrapperRef;
    }

    setBarRef = barRef => {
        this.barRef = barRef;
    }

    show () {

        const {vertical} = this.props;

        const elemHeight = this.wrapperRef.offsetHeight;

        const coordinate =
        matchWindowMedia.minWidth('$media-small-width')
            ? `${BASE_TOP_BOTTOM}px`
            : 0;

        this.wrapperRef.classList.remove(HIDDEN_CLASS);

        if (vertical === 'bottom') {
            this.wrapperRef.style.top = 'auto';
            this.wrapperRef.style.bottom = `-${elemHeight}px`;
            this.wrapperRef.style.bottom = coordinate;
        }

        if (vertical === 'top') {
            this.wrapperRef.style.bottom = 'auto';
            this.wrapperRef.style.top = `-${elemHeight}px`;
            this.wrapperRef.style.top = coordinate;
        }
    }

    hide () {
        const elemHeight = this.wrapperRef.offsetHeight;
        const {vertical} = this.props;

        if (vertical === 'bottom') {
            this.wrapperRef.style.bottom = `-${elemHeight}px`;
        }

        if (vertical === 'top') {
            this.wrapperRef.style.top = `-${elemHeight}px`;
        }

        this.wrapperRef.classList.add(HIDDEN_CLASS);

    }

    setBarCssClass () {
        const {type} = this.props;

        const barClass = smartClass('au-snack-bar', {
            'au-snack-bar_success': type === 'success',
            'au-snack-bar_warning': type === 'warning',
            'au-snack-bar_error': type === 'error',
        });

        this.barRef.className = barClass;
    }

    render () {

        const {mounted, content, vertical, horizontal, onClose} = this.props;

        const wrapperClass = smartClass('au-snack-bar-wrapper au-snack-bar-wrapper_transition', {
            [HIDDEN_CLASS]: !mounted,
            'au-snack-bar-wrapper_bottom': vertical === 'bottom',
            'au-snack-bar-wrapper_top': vertical === 'top',
            'au-snack-bar-wrapper_center': horizontal === 'center',
            'au-snack-bar-wrapper_left': horizontal === 'left',
            'au-snack-bar-wrapper_right': horizontal === 'right',
        });

        return (
            <div
                className={wrapperClass}
                ref={this.setWrapperRef}>
                <div ref={this.setBarRef}>
                    {
                        onClose
                            ? <SnackBarClose content={content} onClose={onClose} />
                            : content
                    }
                </div>
            </div>
        );
    }

}

SnackBar.propTypes = {
    mounted: PropTypes.bool,
    show: PropTypes.bool,
    type: PropTypes.string,
    vertical: PropTypes.string,
    horizontal: PropTypes.string,
    onClose: PropTypes.func,
};

SnackBar.defaultProps = {
    type: 'default',
    vertical: 'bottom',
    horizontal: 'right',
};

class SnackBarStateProvider extends PureComponent {

    state = {
        show: false,
        mounted: false,
    };

    timerId = null;

    componentDidMount () {
        document.addEventListener('keydown', this._handleKeydown);
        this.setState({mounted: true});
    }

    componentWillUnmount () {
        document.removeEventListener('keydown', this._handleKeydown);
    }

    componentDidUpdate () {

        const {
            hideDelay: curDelay,
            show: curShow,
        } = this.state;

        this.stopTimer();

        if (curShow && curDelay) {
            this.timed(this._getCloseListener() || this.hide, curDelay);
        }
    }

    stopTimer () {
        if (this.timerId) {
            clearTimeout(this.timerId);
            this.timerId = null;
        }
    }

    timed (callback, delay) {
        this.timerId = setTimeout(callback, delay);
    }

    hide = () => {

        if (!this.state.show) {
            return;
        }
        this.stopTimer();
        this.setState({show: false});
    }

    _getCloseListener () {
        if (typeof this.state.onClose === 'function') {
            return this.state.onClose;
        }

        if (this.state.showClose === true) {
            return this.hide;
        }

        return null;
    }

    _handleKeydown = e => {

        if (!this.state.show) {
            return;
        }

        const closeAction = this._getCloseListener();

        if (closeAction) {
            escKeyAction(e, closeAction);
        }

    }

    render () {

        /*eslint-disable no-unused-vars */
        const {hideDelay, onClose, ...otherState} = this.state;
        /*eslint-enable no-unused-vars */

        return (
            <SnackBar
                onClose={this._getCloseListener()}
                {...otherState} />
        );
    }
}

export {SnackBarStateProvider, SnackBar};
