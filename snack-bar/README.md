# Панель уведомлений

Аналог material design snackbar. Компонент-портал, единственный экземпляр находится в body, доступ происходит через компонент-интерфейс

```
<SnackBarContainer
    show
    hideDelay={5000}
    type='success'
    content='Всё хорошо' />
```
