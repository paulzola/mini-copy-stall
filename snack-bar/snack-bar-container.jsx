'use strict';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

import {SnackBarStateProvider} from './snack-bar';

class SnackBarDomComponent {

    componentRef = null;

    createComponent (callback) {

        if (this.creating) {
            return;
        }

        this.creating = true;

        const container = document.createElement('div');
        document.body.appendChild(container);

        ReactDOM.render(
            <div className='au-snack-bar-container'>
                <SnackBarStateProvider ref={componentRef => this.componentRef = componentRef} />
            </div>,
            container,
            () => {
                this.creating = false;
                callback(this.componentRef);
            },
        );
    }

    getComponent (callback) {

        if (this.componentRef) {
            callback(this.componentRef);
            return;
        }

        this.createComponent(callback);
    }

}



class SnackBarContainer extends PureComponent {

    componentDidMount () {
        this.giveState(this.props);
    }

    componentDidUpdate () {
        this.giveState(this.props);
    }

    /*eslint-disable complexity*/
    giveState (state) {

        if (!window['snackBarDomComponent']) {
            window['snackBarDomComponent'] = new SnackBarDomComponent();
        }

        const {
            onClose: curOnClose,
            hideDelay: curDelay,
            showClose: curShowClose,
            type: curType,
            ...otherState} = state;

        const onClose = curOnClose ? curOnClose : null;
        const showClose = curShowClose ? curShowClose : false;
        const hideDelay = curDelay ? curDelay : null;
        const type = curType ? curType : null;
        const newState = {
            onClose,
            hideDelay,
            showClose,
            type,
            ...otherState};

        window['snackBarDomComponent'].getComponent(componentRef => componentRef.setState(newState));
    }
    /* eslint-enable complexity */

    render () {
        return null;
    }

}

SnackBarContainer.propTypes = {
    show: PropTypes.bool,
    showClose: PropTypes.bool,
    type: PropTypes.string,
    vertical: PropTypes.string,
    horizontal: PropTypes.string,
    onClose: PropTypes.func,
    hideDelay: PropTypes.number,
};

export {SnackBarContainer};
