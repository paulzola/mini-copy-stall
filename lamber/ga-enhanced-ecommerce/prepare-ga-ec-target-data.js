'use strict';

const convertColonSemiColonStringToObject =
require('../helpers/convert-colon-semi-colon-string-to-object');
const validateGaTrackerId = require('../helpers/validate-ga-tracker-id');
const cutName = require('../helpers/cut-name');
const pageListName = require('../helpers/page-list-name');
const {getPageAlias} = require('./page-alias-dictionary');


const lamberCommonDataFactory = data => {
    const {group} = data;
    return {group};
};

const lamberTargetDataFactory = data => {
    const {target, customData, nameContent, groups} = data;
    return {target, customData, nameContent, groups};
};

const listFactory = (list, page) => {
    const {name, alias, type} = list;
    return {
        name: pageListName(page, alias || name),
        type,
    };
};

const gaEcDataFactory = data => {
    const {trackerId, itemId, itemName, itemPrice, list, page, action} = data;
    const pageAlias = getPageAlias(page);
    return {
        trackerId,
        itemId,
        itemName: cutName(itemName),
        itemPrice,
        list: listFactory(list, pageAlias),
        page: pageAlias,
        action,
    };
};

const checkGaEcData = ({item, gaTrackerId, group}) => {
    if (group.indexOf('admin.') === 0) {
        return false;
    }
    if (item && validateGaTrackerId(gaTrackerId)) {
        return true;
    }
    return false;
};

const lamberDataToGaEcData = data => {

    let result = [];

    const {target, customData, nameContent, groups, group, action} = data;

    const parsedTarget = convertColonSemiColonStringToObject(target);
    const parsedCustomData = convertColonSemiColonStringToObject(customData);

    const {item} = parsedTarget;
    const {gaTrackerId, price} = parsedCustomData;

    if (!checkGaEcData({item, gaTrackerId, group})) {
        return result;
    }

    result = groups.map(
        targetGroup =>
            gaEcDataFactory({
                trackerId: gaTrackerId,
                itemId: item,
                itemName: nameContent,
                itemPrice: price,
                list: targetGroup,
                page: group,
                action,
            })
    );

    return result;

};

const prepareGaEcTargetShowData = (action, data) => {
    const commonData = lamberCommonDataFactory(data);
    const {targets} = data;

    let result = [];
    for (let i = 0, l = targets.length; i < l; i++) {
        const targetData = lamberTargetDataFactory(targets[i]);
        const gaEcData = lamberDataToGaEcData({
            ...commonData,
            ...targetData,
            action,
        });
        if (gaEcData) {
            result = result.concat(gaEcData);
        }
    }

    return result;
};

const prepareGaEcTargetClickData = (action, data) => {
    const commonData = lamberCommonDataFactory(data);
    const targetData = lamberTargetDataFactory(data);
    const gaEcData = lamberDataToGaEcData({
        ...commonData,
        ...targetData,
        action,
    });
    return gaEcData;
};

module.exports = {prepareGaEcTargetShowData, prepareGaEcTargetClickData};
