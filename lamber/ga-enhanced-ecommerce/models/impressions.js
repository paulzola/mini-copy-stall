'use strict';

const commonEventData = require('./common-event-data');

const impressionsModel = data => {
    const commonData = commonEventData(data);
    const {
        impressions,
    } = data;
    return {
        ...impressions,
        ...commonData,
    };
};

module.exports = impressionsModel;
