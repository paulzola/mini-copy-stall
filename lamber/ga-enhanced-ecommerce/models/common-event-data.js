'use strict';

const commonEventData = data => {
    const {
        trackerId,
        clientId,
        hitType = 'event',
        eventAction,
        eventCategory,
        eventLabel,
        currencyCode,
    } = data;
    const result = {
        tid: trackerId,
        cid: clientId,
        t: hitType,
        ea: eventAction,
        ec: eventCategory,
        el: eventLabel,
    };
    if (currencyCode) {
        result.cu = currencyCode;
    }
    return result;
};

module.exports = commonEventData;
