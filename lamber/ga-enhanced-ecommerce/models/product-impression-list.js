'use strict';

const productImpressionListModel = data => {
    const {
        index,
        name,
    } = data;
    return {
        [`il${index}nm`]: name,
    };
};

module.exports = productImpressionListModel;
