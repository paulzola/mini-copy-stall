'use strict';

const promotionImpressionModel = data => {
    const {
        impressionListIndex,
        creative,
    } = data;
    const prefix = `promo${impressionListIndex}`;
    return {
        [`${prefix}cr`]: creative,
    };
};

module.exports = promotionImpressionModel;
