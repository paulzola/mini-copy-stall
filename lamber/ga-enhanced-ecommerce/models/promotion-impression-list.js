'use strict';

const promotionImpressionListModel = data => {
    const {
        index,
        name,
    } = data;
    return {
        [`promo${index}nm`]: name,
    };
};

module.exports = promotionImpressionListModel;
