'use strict';

const commonEventData = require('./common-event-data');

const promotionClickModel = data => {
    const commonData = commonEventData(data);
    const {
        promotionAction = 'click',
        promotionCreative,
        promotionId,
        promotionName,
    } = data;
    const result = {
        promoa: promotionAction,
        promo1cr: promotionCreative,
        promo1nm: promotionName,
        ...commonData,
    };
    if (promotionId) {
        result.promo1id = promotionId;
    }
    return result;
};

module.exports = promotionClickModel;
