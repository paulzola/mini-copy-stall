'use strict';

const commonEventData = require('./common-event-data');

const listClickModel = data => {
    const commonData = commonEventData(data);
    const {
        productAction = 'click',
        productActionList,
        productId,
        productName,
        productPrice,
    } = data;
    const result = {
        pa: productAction,
        pal: productActionList,
        pr1id: productId,
        pr1nm: productName,
        ...commonData,
    };
    if (productPrice !== undefined) {
        result.pr1pr = productPrice;
    }
    return result;
};

module.exports = listClickModel;
