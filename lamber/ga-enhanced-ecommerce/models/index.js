'use strict';

const listClickModel = require('./list-click');
const promotionClickModel = require('./promotion-click');
const impressionsModel = require('./impressions');
const productImpressionListModel = require('./product-impression-list');
const productImpressionModel = require('./product-impression');
const promotionImpressionListModel = require('./promotion-impression-list');
const promotionImpressionModel = require('./promotion-impression');
const commonGaEcData = require('./common-ga-ec-data');

module.exports = {
    listClickModel,
    promotionClickModel,
    impressionsModel,
    productImpressionListModel,
    productImpressionModel,
    promotionImpressionListModel,
    promotionImpressionModel,
    commonGaEcData,
};
