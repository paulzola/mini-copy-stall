'use strict';

const getGaClientId = require('../../helpers/ga-client-id');

let data = null;

const model = ({clientId, eventCategory}) => ({
    clientId,
    eventCategory,
});

const commonGaEcData = () => {
    if (!data) {
        data = model({
            clientId: getGaClientId(),
            eventCategory: 'au-ga-ecommerce',
        });
    }
    return data;
};

module.exports = commonGaEcData;
