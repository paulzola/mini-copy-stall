'use strict';

const productImpressionModel = data => {
    const {
        impressionListIndex,
        productImpressionIndex,
        productImpressionId,
        productImpressionName,
        productImpressionPrice,
    } = data;
    const prefix = `il${impressionListIndex}pi${productImpressionIndex}`;
    const result = {
        [`${prefix}id`]: productImpressionId,
        [`${prefix}nm`]: productImpressionName,
    };
    if (productImpressionPrice !== undefined) {
        result[`${prefix}pr`] = productImpressionPrice;
    }
    return result;
};

module.exports = productImpressionModel;
