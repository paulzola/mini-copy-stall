'use strict';

const pageAliasDictionary = require('./dictionary');

const getPageAlias = name => {
    const alias = pageAliasDictionary[name];
    if (alias) {
        return alias;
    }

    return name;
};

module.exports = getPageAlias;
