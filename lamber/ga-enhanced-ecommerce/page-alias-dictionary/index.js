'use strict';

const getPageAlias = require('./get-page-alias');

module.exports = {
    getPageAlias,
};
