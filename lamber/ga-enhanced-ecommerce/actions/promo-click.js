'use strict';

const {promotionClickModel, commonGaEcData} = require('../models');
const sendAction = require('./send-action');

const mapper = data => {
    const {trackerId, itemId, itemName, list, page} = data;
    const sendData = promotionClickModel({
        trackerId,
        eventAction: 'click',
        eventLabel: page,
        promotionAction: 'click',
        promotionCreative: itemName || itemId,
        promotionName: list.name,
        ...commonGaEcData(),
    });
    return sendData;
};


const promoClick = ({send}) => sendAction(mapper, send);

module.exports = promoClick;
