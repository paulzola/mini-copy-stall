'use strict';

class Unload {
    constructor () {
        this.callbacks = [];
        this.addCallback = this.addCallback.bind(this);
    }
    addCallback (c) {
        this.callbacks.push(c);
    }
    run () {
        this.callbacks.forEach(callback => callback());
    }
}

module.exports = new Unload();
