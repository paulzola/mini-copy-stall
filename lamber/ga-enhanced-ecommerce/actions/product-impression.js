'use strict';

const {
    productImpressionListModel,
    productImpressionModel,
    impressionsModel,
    commonGaEcData,
} = require('../models');

const DataDebounceAggregator = require('../../helpers/data-debounce-aggregator');

const initTrackerImpressionsSpread = page => ({
    lists: {},
    page,
});

const spreadImpressionsByTrackersLists = impressions => {
    const result = {};
    impressions.forEach(target => {
        const {trackerId, page, list} = target;

        if (!result[trackerId]) {
            result[trackerId] = initTrackerImpressionsSpread(page);
        }

        const listName = list.name;

        if (!result[trackerId].lists[listName]) {
            result[trackerId].lists[listName] = [];
        }
        result[trackerId].lists[listName].push(target);
    });
    return result;
};

const createImpressionDataForList = ({listName, listIndex, impressions}) => {

    const impressionsDataCollector = {};
    const listIndexForModel = listIndex + 1;

    const productList = productImpressionListModel({
        index: listIndexForModel,
        name: listName,
    });

    Object.assign(impressionsDataCollector, productList);

    impressions.forEach((target, targetIndex) => {
        const {itemId, itemName, itemPrice} = target;
        const targetIndexForModel = targetIndex + 1;
        const impression = productImpressionModel({
            impressionListIndex: listIndexForModel,
            productImpressionIndex: targetIndexForModel,
            productImpressionId: itemId,
            productImpressionName: itemName,
            productImpressionPrice: itemPrice,
        });
        Object.assign(impressionsDataCollector, impression);
    });

    return impressionsDataCollector;

};

const collectTrackerImpressions = impressionsListsByTrackers => {

    const result = [];
    const impressionsDataCollector = {};

    const trackersIds = Object.keys(impressionsListsByTrackers);

    trackersIds.forEach(trackerId => {

        const {lists, page} = impressionsListsByTrackers[trackerId];

        const listsNames = Object.keys(lists);

        listsNames.forEach((listName, listIndex) => {
            const impressions = lists[listName];
            const listImpressions = createImpressionDataForList({
                listName, listIndex, impressions,
            });
            Object.assign(impressionsDataCollector, listImpressions);
        });

        const impressionDataForTracker = impressionsModel({
            trackerId,
            eventAction: 'impressions',
            eventLabel: page,
            impressions: impressionsDataCollector,
            currencyCode: 'RUB',
            ...commonGaEcData(),
        });

        result.push(impressionDataForTracker);

    });

    return result;

};

const productImpression = ({send, releaseDelay, registerForceSend}) => {
    const productImpressionAggregator = new DataDebounceAggregator({
        releaseDelay,
        onRelease: data => {
            if (data && data.length) {
                const trackerImpressions = collectTrackerImpressions(spreadImpressionsByTrackersLists(data));
                trackerImpressions.forEach(impression => send(impression));
            }
        },
    });
    registerForceSend(productImpressionAggregator.release);
    return data => productImpressionAggregator.add(data);
};

module.exports = productImpression;
