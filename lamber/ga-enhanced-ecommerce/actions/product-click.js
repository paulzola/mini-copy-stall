'use strict';

const {listClickModel, commonGaEcData} = require('../models');
const sendAction = require('./send-action');

const mapper = data => {
    const {trackerId, itemId, itemName, itemPrice, list, page} = data;
    const sendData = listClickModel({
        trackerId,
        eventAction: 'click',
        eventLabel: page,
        currencyCode: 'RUB',
        productAction: 'click',
        productActionList: list.name,
        productId: itemId,
        productName: itemName,
        productPrice: itemPrice,
        ...commonGaEcData(),
    });
    return sendData;
};

const productClick = ({send}) => sendAction(mapper, send);

module.exports = productClick;
