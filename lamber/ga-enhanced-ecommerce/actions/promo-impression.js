'use strict';

const {
    promotionImpressionListModel,
    promotionImpressionModel,
    impressionsModel,
    commonGaEcData,
} = require('../models');

const DataDebounceAggregator = require('../../helpers/data-debounce-aggregator');

const initTrackerImpressionsSpread = page => ({
    impressions: [],
    page,
});

const spreadImpressionsByTrackers = impressions => {
    const result = {};
    impressions.forEach(target => {
        const {trackerId, page} = target;
        if (!result[trackerId]) {
            result[trackerId] = initTrackerImpressionsSpread(page);
        }
        result[trackerId].impressions.push(target);
    });
    return result;
};

const createPromoImpressionData = ({target, targetIndex}) => {

    const {itemName, itemId, list} = target;

    const targetIndexForModel = targetIndex + 1;

    const promoList = promotionImpressionListModel({
        index: targetIndexForModel,
        name: list.name,
    });

    const promoImpression = promotionImpressionModel({
        impressionListIndex: targetIndexForModel,
        creative: itemName || itemId,
    });

    const impressionData = Object.assign({}, promoList, promoImpression);
    return impressionData;

};

const collectTrackerImpressions = impressionsByTrackers => {

    const result = [];

    const trackersIds = Object.keys(impressionsByTrackers);

    trackersIds.forEach(trackerId => {
        const {impressions, page} = impressionsByTrackers[trackerId];
        impressions.forEach((target, targetIndex) => {

            const impressionData = createPromoImpressionData({
                target,
                targetIndex,
            });

            const trackerPromoImpressions = impressionsModel({
                trackerId,
                eventAction: 'impressions',
                eventLabel: page,
                impressions: impressionData,
                ...commonGaEcData(),
            });

            result.push(trackerPromoImpressions);
        });
    });

    return result;

};

const promoImpression = ({send, releaseDelay, registerForceSend}) => {
    const promoImpressionAggregator = new DataDebounceAggregator({
        releaseDelay,
        onRelease: data => {
            if (data && data.length) {
                const trackersImpressions = collectTrackerImpressions(spreadImpressionsByTrackers(data));
                trackersImpressions.forEach(impression => send(impression));
            }

        },
    });
    registerForceSend(promoImpressionAggregator.release);
    return data => promoImpressionAggregator.add(data);
};

module.exports = promoImpression;
