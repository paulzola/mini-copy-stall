'use strict';

const productClick = require('./product-click');
const promoClick = require('./promo-click');
const productImpression = require('./product-impression');
const promoImpression = require('./promo-impression');
const unload = require('./unload');

module.exports = {
    productClick,
    promoClick,
    productImpression,
    promoImpression,
    unload,
};
