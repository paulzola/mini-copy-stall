'use strict';

const sendAction = (mapper, send) => data => send(mapper(data));

module.exports = sendAction;
