'use strict';

const actionToItem = require('../helpers/action-to-item');
const conditionalAction = require('../helpers/conditional-action');
const conditionToItem = require('../helpers/condition-to-item');

const sendXhr = require('../queued-xhr/send-xhr.js')(XMLHttpRequest);
const queuedXhr = require('../queued-xhr/queued-xhr.js')({
    sendXhr,
    maxProcessingRequests: 4,
});

const gaMeasurementProvider =
require('./ga-measurement-provider.js')({
    requestQueue: queuedXhr.doQueuedRequest,
    request: queuedXhr.doRequest,
});

const {
    productClick,
    promoClick,
    productImpression,
    promoImpression,
    unload: unloadAction,
} = require('./actions');

const {prepareGaEcTargetClickData, prepareGaEcTargetShowData} = require('./prepare-ga-ec-target-data');

const {
    isPromoTarget,
    isProductTarget,
    isClick,
    isShow,
} = require('./filters');


const processProductClick = conditionalAction({
    condition: data => isClick(data) && isProductTarget(data),
    action: productClick({
        send: gaMeasurementProvider.collect,
    }),
});

const processPromoClick = conditionalAction({
    condition: data => isClick(data) && isPromoTarget(data),
    action: promoClick({
        send: gaMeasurementProvider.collect,
    }),
});

const processProductShow = conditionalAction({
    condition: data => isShow(data) && isProductTarget(data),
    action: productImpression({
        send: gaMeasurementProvider.collectQueued,
        releaseDelay: 150,
        registerForceSend: unloadAction.addCallback,
    }),
});

const processPromoShow = conditionalAction({
    condition: data => isShow(data) && isPromoTarget(data),
    action: promoImpression({
        send: gaMeasurementProvider.collectQueued,
        releaseDelay: 150,
        registerForceSend: unloadAction.addCallback,
    }),
});

const conditionAction = conditionToItem([
    processProductClick,
    processPromoClick,
    processProductShow,
    processPromoShow,
]);

const processTargets = actionToItem(conditionAction);

const unload = () => {
    unloadAction.run();
};

const show = (action, data) => {
    const showTargets = prepareGaEcTargetShowData(action, data);
    if (!showTargets.length) {
        return;
    }
    processTargets(showTargets);
};

const click = (action, data) => {
    const clickTargets = prepareGaEcTargetClickData(action, data);
    if (!clickTargets.length) {
        return;
    }
    processTargets(clickTargets);
};

const actionsHandlers = {
    unload,
    show,
    click,
};

const gaEcSender = (action, data) => {
    const handler = actionsHandlers[action];
    if (handler) {
        handler(action, data);
    }
};

module.exports = gaEcSender;
