'use strict';

const getQueryString =
require('../helpers/parameterized-get-query-string.js');

const makeRequest = request => measurement => {

    const params = {v: 1, t: 'event'};

    const dataWithConstants = Object.assign({}, params, measurement);

    const query = getQueryString({
        address: '//www.google-analytics.com',
        action: 'collect',
        data: dataWithConstants,
        paramsInQueryEnd: {z: Date.now()},
    });

    request({
        method: 'GET',
        query,
    });
};

const gaMeasurementProvider = ({requestQueue, request}) => ({
    collectQueued: makeRequest(requestQueue),
    collect: makeRequest(request),
});

module.exports = gaMeasurementProvider;

