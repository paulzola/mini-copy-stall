'use strict';

const isShow = ({action}) => action === 'show';

module.exports = isShow;
