'use strict';

const isProductTarget = ({list}) => {
    if (!list) {
        return false;
    }
    const {type} = list;
    return type === 'product' || !type;
};

module.exports = isProductTarget;

