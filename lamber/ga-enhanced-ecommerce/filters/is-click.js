'use strict';

const isClick = ({action}) => action === 'click';

module.exports = isClick;
