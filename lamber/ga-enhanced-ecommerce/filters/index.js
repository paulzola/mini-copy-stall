'use strict';

const isPromoTarget = require('./is-promo-target');
const isProductTarget = require('./is-product-target');
const isClick = require('./is-click');
const isShow = require('./is-show');

module.exports = {
    isPromoTarget,
    isProductTarget,
    isClick,
    isShow,
};
