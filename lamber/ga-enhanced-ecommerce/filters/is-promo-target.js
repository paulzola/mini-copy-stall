'use strict';

const isPromoTarget = ({list}) => list && list.type === 'promo';

module.exports = isPromoTarget;
