'use strict';

const objToGetQuery = obj => {
    let str = '';
    for (const key in obj) {
        if (!obj.hasOwnProperty(key)) {
            continue;
        }
        if (str !== '') {
            str += '&';
        }
        str += `${key }=${encodeURIComponent(obj[key])}`;
    }
    return str;
};

const getQueryString = ({address, action, data, paramsInQueryEnd}) => {
    const queryString = objToGetQuery(data);
    const queryStringEnd = paramsInQueryEnd ? `&${objToGetQuery(paramsInQueryEnd)}` : '';
    return `${address}/${action}?${queryString}${queryStringEnd}`;
};

module.exports = getQueryString;
