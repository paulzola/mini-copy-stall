'use strict';

const conditionalAction = ({condition, action}) => data => {
    const result = condition(data);
    if (result) {
        action(data);
    }
    return result;
};

module.exports = conditionalAction;
