'use strict';

const conditionToItem = conditions => item => {
    for (let i = 0, l = conditions.length; i < l; i++) {
        if (conditions[i](item)) {
            break;
        }
    }
};

module.exports = conditionToItem;
