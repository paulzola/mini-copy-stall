'use strict';

const checkNoValue = v => v === undefined || v === null;

const convertColonSemiColonStringToObject = str => {
    const result = {};
    if (!str) {
        return result;
    }
    const keyValues = str.split(';');
    for (let i = 0, l = keyValues.length; i < l; i++) {
        const keyValue = keyValues[i].split(':');

        if (checkNoValue(keyValue[0]) || checkNoValue(keyValue[1])) {
            continue;
        }

        result[keyValue[0]] = keyValue[1];
    }

    return result;
};

module.exports = convertColonSemiColonStringToObject;
