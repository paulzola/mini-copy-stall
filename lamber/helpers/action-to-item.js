'use strict';

const actionToItem = action => items => items.forEach(action);

module.exports = actionToItem;
