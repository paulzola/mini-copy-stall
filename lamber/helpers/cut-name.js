'use strict';

const MAX_LENGTH = 200;

const cutName = (name, maxLength = MAX_LENGTH) => {
    if (name.length > maxLength) {
        let cutted = name.substring(0, maxLength - 1);
        if (name.charAt(maxLength) === ' ') {
            return cutted;
        }
        const lastSpaceIndex = cutted.lastIndexOf(' ');
        if (lastSpaceIndex) {
            cutted = cutted.substring(0, lastSpaceIndex);
        }
        return cutted;
    }
    return name;
};

module.exports = cutName;
