'use strict';

class DataDebounceAggregator {
    constructor ({releaseDelay, onRelease}) {
        this.reset();
        this.timer = null;
        this.releaseDelay = releaseDelay;
        this.onRelease = onRelease;
        this.release = this.release.bind(this);
    }

    add (data) {
        if (this.timer) {
            clearTimeout(this.timer);
            this.timer = null;
        }
        this.store.push(data);
        this.timer = setTimeout(this.release.bind(this), this.releaseDelay);
    }

    getStore () {
        return this.store.slice();
    }

    reset () {
        this.store = [];
    }

    release () {
        const data = this.getStore();
        this.reset();
        this.onRelease(data);
    }
}

module.exports = DataDebounceAggregator;
