'use strict';

const pageListName = (page, list) => `${page}: ${list}`;

module.exports = pageListName;
