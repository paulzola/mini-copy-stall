'use strict';

class GaClientId {

    constructor () {
        this.cid = null;
        this.get = this.get.bind(this);
    }

    getFromCookie () {
        const cookie = document.cookie;
        const gaUserIdParam = '_ga=GA1.2.';
        const gaUserIdParamLength = gaUserIdParam.length;
        const gaUserIdIndex = cookie.indexOf(gaUserIdParam);
        const gaUserIdSemiColonIndex = cookie.indexOf(';', gaUserIdIndex);
        const cid = cookie.substring(gaUserIdIndex + gaUserIdParamLength, gaUserIdSemiColonIndex);
        return cid;
    }

    get () {
        if (this.cid) {
            return this.cid;
        }
        this.cid = this.getFromCookie();
        return this.cid;
    }
}

let gaClientId = null;

const getGaClientId = () => {
    if (!gaClientId) {
        gaClientId = new GaClientId();
    }
    return gaClientId.get();
};

module.exports = getGaClientId;
