'use strict';

const gaTrackerIdRegex = /\bUA-\d+-\d+\b/g;

const validateGaTrackerId = trackerId => {
    if (typeof trackerId !== 'string') {
        return false;
    }
    const test = trackerId.match(gaTrackerIdRegex);
    if (test && test.length === 1) {
        return true;
    }

    return false;
};

module.exports = validateGaTrackerId;

