'use strict';

const sendXhr = Xhr => ({method, query, data, onSuccess, onError, timeout = 10000}) => {

    const xhr = new Xhr();
    xhr.timeout = timeout;

    xhr.open(method, query, true);

    xhr.onreadystatechange = function (res) {
        if (this.readyState !== 4) {
            return;
        }
        if (this.status !== 200) {
            onError(res);
            return;
        }
        onSuccess(res);
    };

    xhr.ontimeout = function () {
        onError();
    };

    if (typeof data === 'undefined') {
        xhr.send();
    } else {
        xhr.send(data);
    }
};

module.exports = sendXhr;
