'use strict';

class QueuedXhr {

    constructor ({sendXhr, maxProcessingRequests = 2}) {
        this.queue = [];
        this.requestsInProcessing = 0;
        this.maxProcessingRequests = maxProcessingRequests;
        this.sendXhr = sendXhr;
        this.doRequest = this.doRequest.bind(this);
        this.doQueuedRequest = this.doQueuedRequest.bind(this);
    }

    doQueuedRequest (p) {

        if (!p) {
            return;
        }

        const req = this._addQueuedRequestParams(p);

        if (!req) {
            return;
        }

        const {method, query, data, onSuccess, onError} = req;

        this._increaseRequestCount();

        this.doRequest({
            query,
            method,
            data,
            onSuccess: res => {
                this._decreaseRequestCount();
                this._doNextQueuedRequest();
                this._runCallback(onSuccess, res);
            },
            onError: res => {
                this._decreaseRequestCount();
                this._doNextQueuedRequest();
                this._runCallback(onError, res);
            },
        });
    }

    doRequest (p) {

        if (!p) {
            return;
        }

        const {method, query, data, onSuccess, onError} = p;

        this.sendXhr({
            query,
            method,
            data,
            onSuccess: res => {
                this._runCallback(onSuccess, res);
            },
            onError: res => {
                this._runCallback(onError, res);
            },
        });
    }

    _increaseRequestCount () {
        const max = this.maxProcessingRequests;
        this.requestsInProcessing++;
        if (this.requestsInProcessing > max) {
            this.requestsInProcessing = max;
        }
    }

    _decreaseRequestCount () {
        this.requestsInProcessing--;
        if (this.requestsInProcessing < 0) {
            this.requestsInProcessing = 0;
        }
    }

    _addQueuedRequestParams (p) {

        const max = this.maxProcessingRequests;

        if (this.requestsInProcessing === max) {
            this.queue.push(p);
            return null;
        }

        return p;
    }

    _doNextQueuedRequest () {
        const nextReq = this._getQueuedRequestParams();
        if (nextReq) {
            this.doQueuedRequest(nextReq);
        }
    }

    _getQueuedRequestParams () {
        const req = this.queue.shift();

        if (req) {
            return req;
        }

        return null;
    }

    _runCallback (f, data) {
        if (typeof f === 'function') {
            f(data);
        }
    }

}

module.exports = p => new QueuedXhr(p);
