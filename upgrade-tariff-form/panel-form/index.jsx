'use strict';

import './panel-form-style.scss';

import React from 'react';
import PropTypes from 'prop-types';
import TariffsButtonsPanel from './tariffs-buttons-panel';
import UpgradeTariffTitle from './upgrade-tariff-title';
import HelpLink from './help-link';

const TariffControls = ({
    currentTariff,
    tariffs,
    onTariffSelect,
    processingTariffId,
    helpLink}) => tariffs && tariffs.length > 0 &&
        <div>

            <h3 className='au-upgrade-tariff-panel-form__title'>
                <UpgradeTariffTitle tariff={currentTariff} />
            </h3>

            <div className='au-upgrade-tariff-panel-form__tariffs'>
                <TariffsButtonsPanel
                    tariffs={tariffs}
                    onTariffSelect={onTariffSelect}
                    processingTariffId={processingTariffId} />
            </div>

            {
                helpLink &&
                <div className='au-upgrade-tariff-panel-form__help'>
                    <HelpLink {...helpLink} />
                </div>
            }

        </div>;


const Load = ({loading}) => loading && <span className='au-icon au-icon_load'></span>;

const UpgradeTariffPanelForm = props => {

    const {
        currentTariff,
        tariffs,
        tariffsLoading,
        onTariffSelect,
        processingTariffId,
        helpLink,
    } = props;

    return (
        <div className='au-upgrade-tariff-panel-form'>

            <Load loading={tariffsLoading} />

            <TariffControls
                currentTariff={currentTariff}
                tariffs={tariffs}
                onTariffSelect={onTariffSelect}
                processingTariffId={processingTariffId}
                helpLink={helpLink} />

        </div>
    );

};

UpgradeTariffPanelForm.propTypes = {
    tariffs: PropTypes.array,
};

UpgradeTariffPanelForm.defaultProps = {
    onTariffSelect: function () {},
};

export default UpgradeTariffPanelForm;
