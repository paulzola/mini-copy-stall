'use strict';

import './tariffs-buttons-panel-style.scss';

import React from 'react';
import ButtonProcessed from '~/components/button-processed';

const TariffButton = props => {

    const {
        tariff,
        onSelect,
        processing,
    } = props;

    return (
        <ButtonProcessed
            processing={processing}
            text={`${tariff.upgradeButtonText || tariff.name}`}
            buttonClass="au-btn_default"
            onClick={() => onSelect(tariff.id)} />
    );

};

const TariffsButtonsPanel = props => {

    const {tariffs, onTariffSelect, processingTariffId} = props;

    return (
        <div className='au-tariffs-buttons-panel'>
            {tariffs.map(t =>
                <div
                    key={t.id}
                    className='au-tariffs-buttons-panel__button'>
                    <TariffButton
                        tariff={t}
                        onSelect={onTariffSelect}
                        processing={t.id === processingTariffId} />
                </div>
                )
            }
        </div>
    );

};

export default TariffsButtonsPanel;
