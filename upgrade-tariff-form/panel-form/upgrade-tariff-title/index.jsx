'use strict';

import './upgrade-tariff-title-style.scss';

import React from 'react';

const UpgradeTariffTitle = ({tariff}) =>
    <span className='au-upgrade-tariff-current-title'>
        {tariff.upgradeTitle}
    </span>;

export default UpgradeTariffTitle;
