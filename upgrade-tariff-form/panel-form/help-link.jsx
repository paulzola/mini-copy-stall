'use strict';

import React from 'react';

const HelpLink = ({text, url}) => <a className='au-btn au-btn_link' href={url}>{text}</a>;

export default HelpLink;
