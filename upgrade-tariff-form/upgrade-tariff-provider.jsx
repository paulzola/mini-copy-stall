'use strict';

import {PureComponent} from 'react';
import PropTypes from 'prop-types';

class UpgradeTariffProvider extends PureComponent {

    state = {
        processingTariffId: false,
        errors: null,
        selectedTariffId: null,
    }

    _processingTariffId = false;

    _handleSelectTariff = selectedTariffId => {

        if (this._processingTariffId) {
            return;
        }

        this.setState({selectedTariffId});
    }

    _handleUnSelectTariff = () => {

        if (this._processingTariffId) {
            return;
        }

        this.setState({selectedTariffId: null});
    }

    _handleUpgradeTariff = () => {

        if (this._processingTariffId) {
            return;
        }

        const {selectedTariffId} = this.state;

        if (!selectedTariffId) {
            return;
        }

        const {requestUpgrade} = this.props;

        const send = () => requestUpgrade({tariffId: selectedTariffId})
            .then(this._handleUpgradeSuccess)
            .catch(this._setErrors);

        this._setProcessingTariffId(selectedTariffId, send);
    }

    _handleUpgradeSuccess = data => {
        const {onUpgradeSuccess} = this.props;
        onUpgradeSuccess(data);
    }

    _setProcessingTariffId (processingTariffId, callback) {
        this._processingTariffId = processingTariffId;
        const newState = {processingTariffId};
        if (processingTariffId) {
            newState.errors = null;
        }
        this.setState(newState, callback);
    }

    _setErrors = errors => {
        this.setState({errors, processingTariffId: null});
        this._processingTariffId = null;
    }

    render () {
        const {
            selectedTariffId,
            processingTariffId,
            errors,
        } = this.state;

        const {render} = this.props;

        return render({
            selectTariff: this._handleSelectTariff,
            unSelectTariff: this._handleUnSelectTariff,
            upgradeTariff: this._handleUpgradeTariff,
            selectedTariffId,
            processingTariffId,
            upgradeTariffErrors: errors,
        });
    }

}

UpgradeTariffProvider.propTypes = {
    onUpgradeSuccess: PropTypes.func,
    requestUpgrade: PropTypes.func.isRequired,
    render: PropTypes.func.isRequired,
};

UpgradeTariffProvider.defaultProps = {
    onUpgradeSuccess: function () {},
};

export default UpgradeTariffProvider;
