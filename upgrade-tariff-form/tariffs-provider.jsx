'use strict';

import {PureComponent} from 'react';
import PropTypes from 'prop-types';

class TariffsProvider extends PureComponent {

    state = {
        currentTariff: null,
        tariffs: [],
        tariffsLoading: true,
        helpLink: null,
    }

    componentDidMount () {
        this._getTariffs();
    }

    _getTariffs () {

        const {getTariffs} = this.props;

        getTariffs()
            .then(result => {
                this.setState(result);
                this._setTariffsLoading(false);
            })
            .catch(() => this._setTariffsLoading(false));

    }

    _setTariffsLoading (tariffsLoading) {
        this.setState({tariffsLoading});
    }

    render () {
        const {
            currentTariff,
            tariffs,
            helpLink,
            tariffsLoading,
        } = this.state;

        const {render} = this.props;

        return render({
            currentTariff,
            tariffs,
            helpLink,
            tariffsLoading,
        });
    }

}

TariffsProvider.propTypes = {
    getTariffs: PropTypes.func.isRequired,
    render: PropTypes.func.isRequired,
};



export default TariffsProvider;
