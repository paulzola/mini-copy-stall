'use strict';

import {fetchJson} from '~/components/js-helpers/fetch';
import {prepareTariff, checkValidTariff} from './prepare-tariffs';

const requestUpgrade = ({tariffId}) => fetchJson('api.tariffs.upgrade', {tariffId});

const tariffsCostRequest = () => fetchJson('tariffs.upgradecost');

const currentTariffRequest = userTariff => new Promise(resolve => {
    resolve(userTariff);
});

const getHelpLink = () => new Promise(resolve => {
    resolve({text: 'Преимущества', url: window.route('help.show', {id: 20})});
});

const getTariffsData = ({userTariff}) => () =>
    Promise.all([
        tariffsCostRequest(),
        currentTariffRequest(userTariff),
        getHelpLink(),
    ]).then(
        result => {
            const [tariffs, currentTariff, helpLink] = result;
            const preparedTariffs = [];
            tariffs.forEach(t => {
                if (checkValidTariff(t)) {
                    preparedTariffs.push(prepareTariff(t));
                }
            });
            return {
                tariffs: preparedTariffs,
                currentTariff: prepareTariff(currentTariff),
                helpLink,
            };
        },
        error => error
    );

export {getTariffsData, requestUpgrade};
