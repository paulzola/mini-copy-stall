'use strict';

import './select-form-style.scss';

import React from 'react';
import PropTypes from 'prop-types';
import OfferButton from '~/components/offer-button';
import Price from '~/components/au-price/react';
import ButtonProcessed from '~/components/button-processed';
import ValidationSummary from '~/components/validation-summary';
import TariffLabel from './tariff-label';

const ErrorsAlert = ({errors}) =>
    <div className="au-alert au-alert_error">
        <ValidationSummary errors={errors} />
    </div>;

const TariffButton = props => {

    const {id, name, cost, onTariffSelect, selectedTariffId, processingTariffId} = props;

    return (
        <OfferButton
            name='upgradeTariff'
            active={id === selectedTariffId}
            disabled={!!processingTariffId}
            fullWidth
            onChange={onTariffSelect}
            value={id}
            valueText={name}
            price={
                <Price
                    supKopek
                    hideZeroKopek
                    price={cost}
                    cssModClass='au-price_current au-price_big_bold' />
            }
        />
    );
};

const UpgradeTariffSelectForm = props => {

    const {
        currentTariff,
        tariffs,
        selectedTariffId,
        onTariffSelect,
        onUpgradeTariff,
        processingTariffId,
        errors,
    } = props;

    return (
        <div className='au-upgrade-tariff-select-form'>

            {
                currentTariff &&
                <div className='au-upgrade-tariff-select-form__section'>
                    <div className='au-label_sm au-upgrade-tariff-select-form__label'>Текущий тариф:</div>
                    <TariffLabel {...currentTariff} />
                </div>
            }

            {
                tariffs && tariffs.length > 0 &&
                <div className='au-upgrade-tariff-select-form__section'>
                    <div className='au-label_sm au-upgrade-tariff-select-form__label'>Изменить на:</div>

                    {
                        tariffs.map(t =>
                            <div key={t.id}>
                                <TariffButton
                                    {...t}
                                    onTariffSelect={onTariffSelect}
                                    selectedTariffId={selectedTariffId}
                                    processingTariffId={processingTariffId} />
                            </div>)
                    }
                </div>
            }

            {
                errors &&
                <div className='au-upgrade-tariff-select-form__section'>
                    <ErrorsAlert errors={errors} />
                </div>
            }

            <div className='au-upgrade-tariff-select-form__section'>
                <ButtonProcessed
                    processing={!!processingTariffId}
                    text='Сменить тариф'
                    buttonClass='au-btn_lg au-btn_wide au-btn_success au-text-bold'
                    iconClass='au-icon_load_white'
                    onClick={onUpgradeTariff} />
            </div>

            {
                currentTariff.upgradeDescription &&
                <div
                    className='au-label_sm au-upgrade-tariff-select-form__description'
                    dangerouslySetInnerHTML={{__html: currentTariff.upgradeDescription}}>
                </div>
            }

        </div>
    );

};

UpgradeTariffSelectForm.propTypes = {
    onTariffSelect: PropTypes.func,
    onUpgradeTariff: PropTypes.func,
    tariffs: PropTypes.array,
};

UpgradeTariffSelectForm.defaultProps = {
    onTariffSelect: function () {},
    onUpgradeTariff: function () {},
};

export default UpgradeTariffSelectForm;
