'use strict';

import './tariff-label-style.scss';

import React from 'react';

const tariffDateText = ({isActive, dateEnd}) => {
    const verbOn = 'подключен до';
    const verb = isActive ? verbOn : `был ${verbOn}`;
    return `${verb} ${window.datify(dateEnd, 'fullDate')}`;
};

const TariffLabel = ({name, dateEnd, isActive}) =>
    <div className='au-tariff-label'>
        <div className='au-tariff-label__title'>{name}</div>
        <div className='au-tariff-label__date'>{tariffDateText({isActive, dateEnd})}</div>
    </div>;

export default TariffLabel;
