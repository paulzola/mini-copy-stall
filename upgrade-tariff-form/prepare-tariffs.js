'use strict';

const COMFORT = 'COMFORT';
const STANDARD = 'STANDARD';
const PREMIUM = 'PREMIUM';

const upgradePriceDescription = 'Указана стоимость заказа нового тарифа на срок до конца действия текущего.';

const tariffsTexts = {
    [COMFORT]: {
        name: 'Комфорт',
        upgradeTitle: 'Не хватает возможностей улучшенного аккаунта?\nВозможно, пришло время стать магазином:',
        upgradeDescription: `<p>${upgradePriceDescription}</p><p>Внимание! После смены тарифа, вернуться на «Комфорт» будет невозможно.</p>`,
    },
    [STANDARD]: {
        name: 'Стандарт',
        upgradeTitle: 'Не хватает возможностей стандартного магазина?',
        upgradeButtonText: 'Перейти на «Стандарт»',
        upgradeDescription: upgradePriceDescription,
    },
    [PREMIUM]: {
        name: 'Премиум',
        upgradeButtonText: 'Перейти на «Премиум»',
        upgradePriceDescription: upgradePriceDescription,
    },
};

const prepareTariff = tariff => ({
    id: tariff.tariffId,
    ...tariff,
    ...tariffsTexts[tariff.tariffId],
});

const checkValidTariff = tariff => !!(tariff && tariff.cost > 0 && tariff.tariffId && tariffsTexts[tariff.tariffId]);

export {prepareTariff, checkValidTariff, COMFORT, STANDARD};
