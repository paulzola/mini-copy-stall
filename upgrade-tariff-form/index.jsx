'use strict';

import './style.scss';

import React from 'react';

import {ModalDialog} from '~/components/modal';
import payRequest from '~/components/pay-request';
import TariffsProvider from './tariffs-provider';
import UpgradeTariffProvider from './upgrade-tariff-provider';
import UpgradeTariffPanelForm from './panel-form';
import UpgradeTariffSelectForm from './select-form';
import {COMFORT, STANDARD} from './prepare-tariffs';
import {getTariffsData, requestUpgrade} from './tariffs-api';

const tariffAvailableToUpgrade = tariff => {
    if (!tariff) {
        return false;
    }
    return tariff.isActive && (tariff.tariffId === STANDARD || tariff.tariffId === COMFORT);
};

const onTariffUpgradeSuccess = data => {

    const {deferredOrderId, amount} = data;

    if (deferredOrderId) {
        const noBalance = {
            amount,
            fromPage: 'myTariff',
            route: 'payment.start.post',
            returnUrl: window.route('my.tariff'),
            callbackUrl: window.route('tariffs.deferredupgrade', {deferredOrderId}),
        };
        payRequest(noBalance);
        return;
    }

    window.location.reload();
};

const thereIsTariffsToShow = ({currentTariff, tariffs, tariffsLoading}) =>
    !!currentTariff ||
    !!(tariffs && tariffs.length > 0) ||
    tariffsLoading;

const renderUpgradeForm = props => {

    const {
        selectedTariffId,
        upgradeTariff,
        selectTariff,
        unSelectTariff,
        processingTariffId,
        upgradeTariffErrors,
        currentTariff,
        tariffs,
        tariffsLoading,
        helpLink,
    } = props;

    return (
        <div className='au-tariff-section au-tariff-section_orange au-tariff-section_upgrade'>
            <UpgradeTariffPanelForm
                currentTariff={currentTariff}
                tariffs={tariffs}
                onTariffSelect={selectTariff}
                processingTariffId={processingTariffId}
                tariffsLoading={tariffsLoading}
                helpLink={helpLink} />
            {
                selectedTariffId &&
                <ModalDialog
                    title='Сменить тариф'
                    onClose={unSelectTariff}>
                    <UpgradeTariffSelectForm
                        currentTariff={currentTariff}
                        tariffs={tariffs}
                        selectedTariffId={selectedTariffId}
                        onTariffSelect={selectTariff}
                        onUpgradeTariff={upgradeTariff}
                        processingTariffId={processingTariffId}
                        errors={upgradeTariffErrors} />
                </ModalDialog>
            }
        </div>
    );

};

const renderTariffs = tariffsProps => {

    if (!thereIsTariffsToShow(tariffsProps)) {
        return null;
    }

    return (
        <UpgradeTariffProvider
            requestUpgrade={requestUpgrade}
            onUpgradeSuccess={onTariffUpgradeSuccess}
            render={upgradeProps => renderUpgradeForm({...tariffsProps, ...upgradeProps})} />
    );

};

const UpgradeTariffForm = props => {

    const {userTariff} = props;

    if (!tariffAvailableToUpgrade(userTariff)) {
        return null;
    }

    const dataRequest = getTariffsData({userTariff});

    return (
        <TariffsProvider
            getTariffs={dataRequest}
            render={renderTariffs} />
    );
};

export default UpgradeTariffForm;
