'use strict';

import './advance-packs-form-group-style.scss';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';

const themes = ['target', 'max', 'lucky'];

class AdvancePacksFormGroup extends PureComponent {

    lastThemeIndex = 0;

    getTheme (themeName) {
        if (themeName) {
            return themeName;
        }
        const theme = themes[this.lastThemeIndex];
        this.lastThemeIndex++;
        if (this.lastThemeIndex >= themes.length) {
            this.lastThemeIndex = 0;
        }
        return theme;
    }

    render () {

        const {
            PackComponent,
            advancePacks,
            packsLoading,
            packsLoadErrors,
            ...other
        } = this.props;

        if (packsLoading) {
            return (
                <span className='au-load-label'>
                    <span className='au-load-label__icon au-icon au-icon_load'></span>
                    <span className='au-load-label__text'>Загрузка пакетов</span>
                </span>
            );
        }

        if (packsLoadErrors) {
            return 'Не удалось загрузить пакеты';
        }

        return (
            <div className='au-advance-pack-form-group'>
                {
                    advancePacks.map(
                        pack =>
                            pack.active
                                ?
                                    <div
                                        className='au-advance-pack-form-group__item'
                                        key={pack.id}>
                                        <PackComponent
                                            pack={pack}
                                            {...other}
                                            themeName={this.getTheme(pack.themeName)} />
                                    </div>
                                : null
                    )
                }
            </div>
        );
    }

}

AdvancePacksFormGroup.propTypes = {
    PackComponent: PropTypes.func.isRequired,
    advancePacks: PropTypes.array,
    packsLoading: PropTypes.bool,
};

export {AdvancePacksFormGroup};
