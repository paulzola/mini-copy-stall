'use strict';

import './advance-pack-days-order-style.scss';

import React from 'react';
import ButtonProcessed from '~/components/button-processed';
import ValidationSummary from '~/components/validation-summary';
import pluralize from '~/components/js-helpers/pluralize';
import Price from '~/components/au-price/react';
import formatTimeCountdown from '~/components/js-helpers/date/format-time-countdown';
import OfferButton from '~/components/offer-button';

const daysPlur = ['день', 'дня', 'дней'];

const takeCostTimeMeasure = day => {
    if (day === -1) {
        return 'До конца торгов';
    }
    return pluralize(day, daysPlur);
};

const getCountdownText = allTimeDateEnd => {
    const {days, hours, minutes} = formatTimeCountdown(allTimeDateEnd);
    return [
        days ? pluralize(days, daysPlur) : '',
        hours ? `${hours} ч` : '',
        minutes ? `${minutes} м` : '',
    ].join(' ');

};

const getButtonPrice = dayCost => {
    const {cost, discountCost} = dayCost;

    if (discountCost) {
        return (
            <Price
                supKopek
                hideZeroKopek
                price={discountCost}
                cssModClass='au-price_current au-price_big_bold' />
        );
    }

    if (cost) {
        return (
            <Price
                supKopek
                hideZeroKopek
                price={cost}
                cssModClass='au-price_big' />
        );
    }

    return null;

};

const AdvancePackOrderButton = props => {

    const {cost, active, disabled, allTimeDateEnd, onChange} = props;

    const days = takeCostTimeMeasure(cost.day);

    const price = getButtonPrice(cost);

    const countdown = cost.day === -1 && cost.active
        ? getCountdownText(allTimeDateEnd)
        : null;

    const discount = cost.dayDiscount
        ? <span className='au-accent au-accent_mini au-advance-pack-days-order-form__discount'>-{cost.dayDiscount}%</span>
        : null;

    return (
        <OfferButton
            name='packCostDay'
            active={active}
            disabled={disabled}
            fullWidth
            onChange={onChange}
            value={cost.day}
            valueText={days}
            price={price}
            specParam={discount}
            infoText={countdown}
        />
    );
};

const onOfferButtonClick = (processing, callback, value) => {
    if (processing) {
        return;
    }
    callback(value);
};

const AdvancePackDaysOrderForm = props => {

    const {
        activeCost,
        pack,
        processing,
        onOrderPack,
        onSelectPackCost,
    } = props;

    return (
        <div className='au-advance-pack-days-order-form'>

            {
                pack.costs.map(c =>
                    c.active &&
                    <AdvancePackOrderButton
                        key={c.day}
                        cost={c}
                        active={activeCost ? c.day === activeCost.day : false}
                        allTimeDateEnd={pack.allTimeDateEnd}
                        disabled={processing}
                        onChange={
                            value => onOfferButtonClick(processing, onSelectPackCost, value)
                        }/>
                )
            }

            <div className='au-advance-pack-days-order-form__submit'>
                <ButtonProcessed
                    processing={processing}
                    text='Заказать'
                    buttonClass='au-btn_primary au-btn_lg au-btn_wide'
                    onClick={onOrderPack} />
            </div>

        </div>
    );

};

const AnotherPackOrdering = () =>
    <span className='au-load-label'>
        <span className='au-load-label__icon au-icon au-icon_load'></span>
        <span className='au-load-label__text'>Идёт заказ другого пакета</span>
    </span>;

const AdvancePackDaysOrderErrors = ({errors, onHideClick}) =>
    <div className='au-advance-pack-days-order__message au-alert au-alert_error'>
        <ValidationSummary
            errors={errors}
            title='Ошибка заказа пакета'
            extraContent={
                <div className='au-advance-pack-days-order__error-close'>
                    <button
                        type='button'
                        className='au-btn au-btn_text au-link au-link_dotted'
                        onClick={onHideClick}>
                        скрыть
                    </button>
                </div>
            } />
    </div>;

const AdvancePackDaysOrder = props => {

    const {
        pack,
        processing,
        successMessage,
        errors,
        onErrorHide,
    } = props;

    if (processing && processing.packageId !== pack.id) {
        return <AnotherPackOrdering />;
    }

    return (
        <div className='au-advance-pack-days-order'>

            {
                typeof successMessage === 'string' &&
                <div className='au-advance-pack-days-order__message au-alert au-alert_success'>
                    {successMessage}
                </div>
            }

            {errors && <AdvancePackDaysOrderErrors errors={errors} onHideClick={onErrorHide} />}

            <AdvancePackDaysOrderForm {...props} />

        </div>
    );
};

export {AdvancePackDaysOrder};
