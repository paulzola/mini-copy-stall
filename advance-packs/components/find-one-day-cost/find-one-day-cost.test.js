'use strict';

const {findDayCost} = require('./index');

const [UnitTest, assert] = require('ruban').Full;

const normalCosts = [
    {
        cost: 10,
        discountCost: 5,
        day: 1,
        active: true,
    },
    {
        cost: 30,
        discountCost: 15,
        day: 3,
        active: true,
    },
    {
        cost: 70,
        discountCost: 35,
        day: 7,
        active: false,
    },
    {
        cost: 100,
        discountCost: 50,
        day: -1,
        active: true,
    },
];

const mixCosts = [
    {
        cost: 30,
        discountCost: 15,
        day: 3,
        active: true,
    },
    {
        cost: 100,
        discountCost: 50,
        day: -1,
        active: true,
    },
    {
        cost: 70,
        discountCost: 35,
        day: 7,
        active: false,
    },
    {
        cost: 10,
        discountCost: 5,
        day: 1,
        active: true,
    },
];

const endDaysCosts = [
    {
        cost: 10,
        discountCost: 5,
        day: 1,
        active: false,
    },
    {
        cost: 100,
        discountCost: 50,
        day: -1,
        active: true,
    },
];

const notActiveCosts = [
    {
        cost: 10,
        discountCost: 5,
        day: 1,
        active: false,
    },
    {
        cost: 100,
        discountCost: 50,
        day: -1,
        active: false,
    },
];

module.exports.FindDayCost = class FindDayCost extends UnitTest {

    *'Если передан номальный массив дней пакета то вернуть цену за день' () {
        const result = findDayCost(normalCosts);
        assert.deepEqual(result, {cost: 10, discountCost: 5, day: 1, active: true});
    }

    *'Если передан смешанный массив дней пакета то вернуть цену за день' () {
        const result = findDayCost(mixCosts);
        assert.deepEqual(result, {cost: 10, discountCost: 5, day: 1, active: true});
    }

    *'Если передан массив дней пакета только до конца торгов, то вернуть стоимость до конца торгов' () {
        const result = findDayCost(endDaysCosts);
        assert.deepEqual(result, {cost: 100, discountCost: 50, day: -1, active: true});
    }

    *'Если передан массив неакативных дней пакета, то вернуть цену за день' () {
        const result = findDayCost(notActiveCosts);
        assert.deepEqual(result, {cost: 10, discountCost: 5, day: 1, active: false});
    }

    *'Если передан пустой массив, то вернуть null' () {
        const result = findDayCost([]);
        assert.equal(result, null);
    }

};
