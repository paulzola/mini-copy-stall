'use strict';

const getCosts = costs => {

    let oneDayCost = null;
    let allTimeCost = null;

    for (let i = 0, l = costs.length; i < l; i++) {

        if (costs[i].day === 1) {
            oneDayCost = costs[i];
        }

        if (costs[i].day === -1) {
            allTimeCost = costs[i];
        }

    }

    return {oneDayCost, allTimeCost};
};

/*eslint-disable complexity*/
const findDayCost = costs => {

    const {oneDayCost, allTimeCost} = getCosts(costs);

    if (oneDayCost && oneDayCost.active) {
        return oneDayCost;
    }

    if (allTimeCost && allTimeCost.active) {
        return allTimeCost;
    }

    return oneDayCost;

};

module.exports = {getCosts, findDayCost};
