'use strict';

import './style.scss';
import React from 'react';
import AdvancePackServicesList from '../advance-pack-services-list';

const AdvancePackInfo = ({pack, orderComponent}) =>
    <div className='au-advance-pack-info'>

        <div className='au-advance-pack-info__section'>
            <div className='au-advance-pack-info__label au-label_sm'>Заказать на срок:</div>
            {orderComponent}
        </div>

        <div className='au-advance-pack-info__section'>
            <div className='au-advance-pack-info__label au-label_sm'>Пакет состоит из услуг:</div>
            <div className='au-advance-pack-info__services-list'>
                <AdvancePackServicesList services={pack.services} />
            </div>
        </div>
        {
            pack.info && <div>
                <div className='au-advance-pack-info__label au-label_sm'>Преимущества:</div>
                <div dangerouslySetInnerHTML={{__html: pack.info}}></div>
            </div>
        }
    </div>;


export {AdvancePackInfo};
