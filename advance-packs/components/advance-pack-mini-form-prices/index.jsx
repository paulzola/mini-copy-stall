'use strict';

import React from 'react';
import Price from '~/components/au-price/react';
import {findDayCost} from '../find-one-day-cost';


const takeCost = costs => {

    const emptyCost = {
        cost: null,
        discountCost: null,
        day: null,
    };

    if (!costs) {
        return emptyCost;
    }

    const currentCost = findDayCost(costs);

    if (!currentCost) {
        return emptyCost;
    }

    return Object.assign(emptyCost, currentCost);
};

const takeCostTimeMeasure = day => {
    switch (day) {
        case 1:
            return 'в сутки';
        case -1:
            return 'до конца торгов';
        default:
            return null;
    }
};

const AdvancePackMiniFormPrices = props => {

    const {costs} = props;

    const c = takeCost(costs);

    return (
        <div className='au-advance-pack-mini-form__prices'>

            {
                c.cost &&
                <div className='au-advance-pack-mini-form__price-old'>
                    <Price
                        price={c.cost}
                        strike={true}
                        cssModClass='au-price_inactive au-price_xs' />
                </div>
            }

            {
                c.discountCost &&
                <div className='au-advance-pack-mini-form__price-new'>
                    <Price
                        price={c.discountCost}
                        supKopek={true}
                        hideZeroKopek={true}
                        cssModClass='au-price_current au-price_large' />
                    {' '}
                    <span className='au-advance-pack-mini-form__price-measure au-price-measure'>
                        {takeCostTimeMeasure(c.day)}
                    </span>
                </div>
            }

        </div>
    );
};

export {AdvancePackMiniFormPrices};
