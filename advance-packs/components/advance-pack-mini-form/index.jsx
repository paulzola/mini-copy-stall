'use strict';

import './advance-pack-mini-form-style.scss';
import './advance-pack-mini-form-content-align.scss';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {smartClass} from 'components/js-helpers/class';
import {getTheme} from './themes';
import {AdvancePackMiniFormPrices} from '../advance-pack-mini-form-prices';
import HelpIcon from '~/components/au-help-icon';

const AdvancePackMiniFormTitle = ({name}) =>
    <h4 className='au-advance-pack-mini-form__title'>
        <span className='au-advance-pack-mini-form__title-text'>
            {name}
        </span>
        &nbsp;
        <button
            type='button'
            title='Подробнее о пакете'
            className='au-advance-pack-mini-form__info-sign'>
            <HelpIcon />
        </button>
    </h4>;


class AdvancePackMiniForm extends PureComponent {

    _handleCardClick = () => {
        const {id, onCardClick} = this.props;
        if (!onCardClick) {
            return;
        }
        onCardClick(id);
    }

    render () {

        const {
            themeName,
            contentAlignInGroup,
            additionalContent,
            pack,
            packViewType,
        } = this.props;

        const {
            name,
            description,
            costs,
        } = pack;

        const theme = getTheme(themeName);

        const cntClass = smartClass('au-advance-pack-mini-form', {
            'au-advance-pack-mini-form_content-align': contentAlignInGroup,
            [theme.containerClass]: theme.containerClass,
            'au-advance-pack-mini-form_view_lot': packViewType === 'lot',
        });

        return (
            <div className={cntClass} onClick={this._handleCardClick}>

                <AdvancePackMiniFormTitle name={name} />

                {
                    description &&
                    <div className='au-advance-pack-mini-form__desc au-label_sm'>
                        {description}
                    </div>
                }

                <div className='au-advance-pack-mini-form__order'>

                    <AdvancePackMiniFormPrices costs={costs} />

                    <div className='au-advance-pack-mini-form__submit'>
                        <button
                            type='button'
                            className={`au-btn ${theme.buttonClass}`}>
                            Заказать
                        </button>
                    </div>

                </div>

                {additionalContent}

            </div>
        );

    }
}

AdvancePackMiniForm.defaultProps = {
    themeName: 'default',
    contentAlignInGroup: false,
};

AdvancePackMiniForm.propTypes = {
    pack: PropTypes.shape({
        id: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]).isRequired,
        name: PropTypes.string,
        description: PropTypes.string,
        services: PropTypes.array,
    }),
    contentAlignInGroup: PropTypes.bool,
    themeName: PropTypes.string,
    onSubmit: PropTypes.func,
    onCardClick: PropTypes.func,
};


export {AdvancePackMiniForm};
