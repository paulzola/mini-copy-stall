'use strict';

const themes = {
    default: {
        containerClass: '',
        buttonClass: 'au-btn_default',
        buttonIconClass: 'au-icon_load',
    },
    target: {
        containerClass: 'au-advance-pack-mini-form_green',
        buttonClass: 'au-btn_success',
        buttonIconClass: 'au-icon_load_white',
    },
    max: {
        containerClass: 'au-advance-pack-mini-form_blue',
        buttonClass: 'au-btn_primary',
        buttonIconClass: 'au-icon_load_white',
    },
    lucky: {
        containerClass: 'au-advance-pack-mini-form_orange',
        buttonClass: 'au-btn_warning',
        buttonIconClass: 'au-icon_load_white',
    },
};

const getTheme = themeName =>
    themeName && themes[themeName] ? themes[themeName] : themes.default;

export {getTheme};
