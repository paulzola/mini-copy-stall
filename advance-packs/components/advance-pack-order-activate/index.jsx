'use strict';

import {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {fetchJson} from '~/components/js-helpers/fetch';
import payRequest from '~/components/pay-request';
import * as stateProvider from '~/components/js-helpers/state-provider';
import {getCosts} from '../find-one-day-cost';

const reBalance = balance => {
    try {
        stateProvider.publish('balance', balance);
    } catch (e) {
        /*eslint-disable no-console */
        console.error(e);
        /*eslint-enable no-console */
    }
};

class AdvancePackOrderActivate extends PureComponent {

    constructor (props) {
        super(props);
        this.state = {
            activeCost: null,
            processing: null,
            successMessage: null,
            errors: null,
        };
    }

    componentDidMount () {
        const costs = this.props.pack.costs;
        const {allTimeCost} = getCosts(costs);
        this.setState({activeCost: allTimeCost.active ? allTimeCost : null});
    }

    _setProcessing = (processing, callback) => {
        this._processing = processing;
        const nState = {processing};
        if (processing) {
            nState.successMessage = null;
            nState.errors = null;
        }
        this.setState(nState, callback);
    }

    _redirectToPayment (deferredOrderId, deferredOrderAmount) {
        const {itemId, fromPage} = this.props;

        const returnUrl = window.route('advance.active', {id: itemId});

        const callbackUrl = window.route('advancepacks.deferred', {
            id: itemId,
            deferredOrderId,
        });

        const noBalance = {
            route: 'payment.start.post',
            amount: deferredOrderAmount,
            fromPage: fromPage,
            returnUrl,
            callbackUrl,
        };

        payRequest(noBalance);
    }

    _handleError = errors => {
        this._setProcessing(null, () => this.setState({errors}));
    }

    _handleErrorHide = () => {
        this.setState({errors: null});
    }

    _handleSuccess = result => {
        const {deferredOrderId, deferredOrderAmount, errors, balance} = result;

        if (errors) {
            this._handleError(errors);
            return;
        }

        if (deferredOrderId) {
            this._redirectToPayment(deferredOrderId, deferredOrderAmount);
            return;
        }
        this._setProcessing(null, () => {

            this.setState({successMessage: {statusText: 'Пакет продвижения заказан'}},
                () => {
                    this.props.loadPacks();
                    reBalance(balance);
                });
        });
    }

    _handleSelectPackCost = value => {
        const costs = this.props.pack.costs;
        const activeCost = costs.find(c => c.day === parseInt(value));
        this.setState({activeCost: activeCost ? activeCost : null});
    }

    _handleOrderPack = () => {

        if (this._processing) {
            return;
        }

        const {itemId, pack} = this.props;
        const {activeCost} = this.state;

        if (!activeCost) {
            return;
        }

        const query = {packageId: pack.id, days: activeCost.day, itemId};

        const process = () => fetchJson('advancepacks.activate', query)
            .then(this._handleSuccess)
            .catch(this._handleError);

        this._setProcessing(query, process);
    }

    render () {

        const {render, ...otherProps} = this.props;

        return render({
            onOrderPack: this._handleOrderPack,
            onSelectPackCost: this._handleSelectPackCost,
            onErrorHide: this._handleErrorHide,
            ...this.state,
            ...otherProps,
        });
    }
}

AdvancePackOrderActivate.defaultProps = {
    fromPage: '',
};

AdvancePackOrderActivate.propTypes = {
    itemId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    fromPage: PropTypes.string,
    render: PropTypes.func.isRequired,
};

export {AdvancePackOrderActivate};
