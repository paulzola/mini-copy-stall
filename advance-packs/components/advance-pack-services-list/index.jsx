'use strict';

import React from 'react';

import './style.scss';
import '~/components/advance-colors/style.scss';
import advanceColors from '~/components/advance-colors';

const AdvancePackServicesList = ({services}) =>
    <ul className='au-advance-pack-services'>
        {
            services.map(s =>
                <li key={s.serviceId} className='au-advance-pack-services__item au-advance-pack-service'>
                    <span className='au-advance-pack-service__label'>
                        {
                            advanceColors[s.serviceId] &&
                                <span
                                    className='au-advance-color-label'
                                    style={{background: advanceColors[s.serviceId]}}>
                                </span>
                        }
                    </span>
                    <span className='au-advance-pack-service__text'>{s.name}</span>
                </li>
            )
        }
    </ul>;

export default AdvancePackServicesList;
