'use strict';

import React from 'react';

import {AdvancePackOrderActivate} from '../advance-pack-order-activate';
import {AdvancePackMiniFormInfoModal} from '../advance-pack-mini-form-info-modal';
import {AdvancePackMiniForm} from '../advance-pack-mini-form';
import {AdvancePackInfo} from '../advance-pack-info';
import {AdvancePackDaysOrder} from '../advance-pack-days-order';

const AdvanceOrderForm = props =>
    <AdvancePackOrderActivate
        {...props}
        render={props =>
            <AdvancePackMiniFormInfoModal
                {...props}
                PackInfo={AdvancePackInfo}
                PackDaysOrder={AdvancePackDaysOrder}
                render={
                    props => <AdvancePackMiniForm {...props} />
                } />
        } />;

export {AdvanceOrderForm};
