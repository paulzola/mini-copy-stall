'use strict';

const calcServicesCosts = services =>
    services.reduce(
        (costs, service) => (
            {
                cost: costs.cost + service.allTimeCost,
                discountCost: costs.discountCost + service.discountAllTimeCost,
            }
        ),
        {cost: 0, discountCost: 0});

export {calcServicesCosts};
