'use strict';

import {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {fetchJson} from '~/components/js-helpers/fetch';

class AdvancePacksLoader extends PureComponent {

    state = {
        advancePacks: [],
        packsLoading: false,
        packsLoadErrors: null,
    }

    componentDidMount () {
        const {advancePacks} = this.state;
        if (advancePacks.length) {
            return;
        }
        this._loadPacks();
    }

    _setProcessing = (packsLoading, callback) => {
        this._packsLoading = packsLoading;
        this.setState({packsLoading}, callback);
    }

    _loadPacks = () => {

        const {itemId, route} = this.props;

        if (this._packsLoading) {
            return;
        }

        const process = () => fetchJson(route, {itemId})
            .then(advancePacks => {
                this.setState({advancePacks});
            })
            .catch(packsLoadErrors => {
                this.setState({packsLoadErrors});
            })
            .then(() => this._setProcessing(false));

        this._setProcessing(true, process);
    }

    render () {
        const {render, ...otherProps} = this.props;
        const loadPacks = this._loadPacks;
        return render({loadPacks, ...this.state, ...otherProps});
    }
}

AdvancePacksLoader.defaultProps = {
    route: 'advancepacks.pack',
};

AdvancePacksLoader.propTypes = {
    route: PropTypes.string.isRequired,
    itemId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    render: PropTypes.func.isRequired,
};

export {AdvancePacksLoader};
