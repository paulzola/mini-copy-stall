'use strict';

import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {ModalDialog} from '~/components/modal';
import {SnackBarContainer} from '~/components/snack-bar';

const InfoModal = ({pack, PackInfo, PackDaysOrder, onClose, ...otherProps}) =>
    <ModalDialog
        title={pack.name}
        onClose={onClose}>
        <PackInfo
            pack={pack}
            orderComponent={
                <PackDaysOrder
                    pack={pack}
                    {...otherProps} />
            }
        />
    </ModalDialog>;


class AdvancePackMiniFormInfoModal extends PureComponent {

    state = {
        infoModalOpen: false,
    }

    componentDidUpdate (prevProps) {

        const {successMessage} = this.props;

        if (
            prevProps.successMessage === null &&
            successMessage &&
            successMessage.statusText) {
            this.setState({
                infoModalOpen: false,
            });
        }
    }

    _handleCloseInfoModal = () => {
        this.setState({infoModalOpen: false});
    }

    _handleOpenInfoModal = () => {
        this.setState({infoModalOpen: true});
    }

    _getModalContent () {

        const {successMessage} = this.props;
        const {infoModalOpen} = this.state;

        if (successMessage && successMessage.statusText) {
            return (
                <SnackBarContainer
                    show
                    hideDelay={5000}
                    type='success'
                    content={successMessage.statusText} />
            );
        }

        if (infoModalOpen) {
            return <InfoModal {...this.props} onClose={this._handleCloseInfoModal} />;
        }

        return null;

    }

    render () {

        const {render, ...otherProps} = this.props;
        const additionalContent = this._getModalContent();

        return render({
            onCardClick: this._handleOpenInfoModal,
            additionalContent,
            ...otherProps});
    }
}

AdvancePackMiniFormInfoModal.propTypes = {
    successMessage: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.shape({
            statusText: PropTypes.string,
        }),
    ]),
    PackInfo: PropTypes.func.isRequired,
    render: PropTypes.func.isRequired,
};

export {AdvancePackMiniFormInfoModal};
