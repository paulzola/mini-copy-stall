'use strict';

import React from 'react';
import guildIconsTypes from './icons-types';
import GuildIcon from './guild-icon';

const getGuildIcons = ({guilds, text, iconClass}) => {

    const guildsIcons = [];

    guilds.forEach(guild => {
        if (!guildIconsTypes[guild.id]) {
            return;
        }
        guildsIcons.push(
            <GuildIcon
                key={guild.id}
                guildId={guild.id}
                title={guild.name}
                expired={guild.expired}
                iconClass={iconClass}
                text={text} />
        );
    });

    return guildsIcons.length ? guildsIcons : null;

};

export default getGuildIcons;
