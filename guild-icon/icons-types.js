'use strict';

const guildIconsTypes = {

    'COMFORT': {
        'normal': 'au-icon_guild-circle-crown au-icon_guild-color_comfort',
        'expired': 'au-icon_guild-circle-crown au-icon_guild-color_expired',
    },

    'STANDARD': {
        'normal': 'au-icon_guild-circle-crown au-icon_guild-color_standard',
        'expired': 'au-icon_guild-circle-crown au-icon_guild-color_expired',
        'normal_text': 'au-icon_guild_text au-icon_guild-color_standard',
        'expired_text': 'au-icon_guild_text au-icon_guild-color_expired',
        'text_default': 'магазин',
    },

    'PREMIUM': {
        'normal': 'au-icon_guild-circle-crown au-icon_guild-color_premium',
        'expired': 'au-icon_guild-circle-crown au-icon_guild-color_expired',
        'normal_text': 'au-icon_guild_text au-icon_guild-color_premium',
        'expired_text': 'au-icon_guild_text au-icon_guild-color_expired',
        'text_default': 'магазин',
    },

    'CLOSED_MARKET': {
        'normal': 'au-icon_guild-circle-crown au-icon_guild-color_inactive',
        'expired': 'au-icon_guild-circle-crown au-icon_guild-color_expired',
        'normal_text': 'au-icon_guild_text au-icon_guild-color_inactive',
        'expired_text': 'au-icon_guild_text au-icon_guild-color_inactive',
        'text_default': 'магазин',
    },

    'PRO': {
        'normal': 'au-icon_pro au-icon_pro_active',
        'expired': 'au-icon_pro au-icon_pro_will_expire',
    },

};

export default guildIconsTypes;
