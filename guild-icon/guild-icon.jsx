'use strict';

import React from 'react';

import guildIconsTypes from './icons-types';

const getGuildType = guildId => guildIconsTypes[guildId];

/*eslint-disable complexity*/
const getGuildText = ({guildType, text}) => {

    const isText = typeof text === 'string';

    if (isText && text.length > 0 && guildType.text_default) {
        return text;
    }

    if (isText && text.length === 0) {
        return null;
    }

    return guildType.text_default || null;

};
/* eslint-enable complexity */

/*eslint-disable complexity*/
const getGuildData = ({guildType, expired, text}) => {

    const iconText = getGuildText({guildType, text});
    let guildClass = '';

    if (iconText) {
        guildClass = guildType[expired ? 'expired_text' : 'normal_text'];
    }

    if (!guildClass) {
        guildClass = guildType[expired ? 'expired' : 'normal'] || null;
    }

    return {
        guildClass,
        iconText,
    };
};

const GuildIcon = ({guildId, title, expired, iconClass, text}) => {

    const guildType = getGuildType(guildId);

    if (!guildType) {
        return null;
    }

    const guildData = getGuildData({guildType, expired, text});

    const {
        guildClass,
        iconText,
    } = guildData;

    let className = null;

    if (guildClass) {
        className = `au-icon ${guildClass}`;
        if (iconClass) {
            className = `${className} ${iconClass}`;
        }
    }


    return <span className={className} title={title || null}>{iconText}</span>;

};

GuildIcon.defaultProps = {
    text: null,
};

export default GuildIcon;
